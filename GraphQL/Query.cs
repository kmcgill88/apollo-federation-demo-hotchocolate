using McChocolate.Models;

namespace McChocolate.GraphQL;

// ReSharper disable once ClassNeverInstantiated.Global
public class Query
{
    public List<Store> GetStores() => Store.GetStores();
    
    
    [GraphQLName("getStoreById")] // Can override the name shown in the graph
    public Store? GetStoreById([ID] string id) => Store.GetStores().Where(it => it.StoreNumber == id).FirstOrDefault();
    
    
    
}