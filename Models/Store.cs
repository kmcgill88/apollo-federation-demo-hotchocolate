namespace McChocolate.Models;

public class Store
{
    public string Name { get; set; }
    
    
    [Key]
    [ID]
    public string StoreNumber { get; set; }

    [ReferenceResolver]
    public static async Task<Store> GetByIdAsync(string StoreNumber) => Store.GetStores().Where(it => it.StoreNumber == StoreNumber).First();
    
    
    public static List<Store> GetStores() =>
        new()
        {
            new Store
            {
                Name = "Grimes",
                StoreNumber = "1"
            },
            new Store
            {
                Name = "Ankeny",
                StoreNumber = "2"
            },
            new Store
            {
                Name = "Polk City",
                StoreNumber = "3"
            },
            new Store
            {
                Name = "Des Moines",
                StoreNumber = "4"
            },
        };
}