namespace McChocolate.Models;

[ExtendServiceType]
public class Item
{
    [ID]
    [Key]
    [External]
    [GraphQLName("id")]
    public string Id { get; set; }

    public Store Store { get; set; }


    [ReferenceResolver]
    public static async Task<Item> GetItem(string id)
    {
        return new Item
        {
            Id = id,
            Store = Models.Store.GetStores().Where(store => store.StoreNumber == id).First()
        };
    }
}